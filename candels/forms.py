from django import forms
from .models import CandlName

class CandelForm(forms.ModelForm):
    class Meta:
        model = CandlName
        fields = '__all__'