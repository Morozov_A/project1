from django.core.management.base import BaseCommand
from candels.models import Body, Color, CandlName, CandlPrices, Flavoring

class Command(BaseCommand):

    def handle(self, *args, **options):
        # удаление всех объектов
        body = Body.objects.create(name='Геометрическая')
        body1 = Body.objects.create(name='Контейнерная')
        body2 = Body.objects.create(name='Силуэт')

        color = Color.objects.create(name='Красный')
        candlprices = CandlPrices.objects.create(name='2')
        candlprices1 = CandlPrices.objects.create(name='3')
        candlprices2 = CandlPrices.objects.create(name='4')

        flavoring = Flavoring.objects.create(name='Кофейный')
        flavoring2 = Flavoring.objects.create(name='Ванильный')


        CandlName.objects.create(name='Тело Девушки', body=body2, color=color, candlprices=candlprices, flavoring=flavoring)
        CandlName.objects.create(name='В стакане', body=body1, color=color, candlprices=candlprices1, flavoring=flavoring2)
        CandlName.objects.create(name='Цилиндрическая', body=body, color=color, candlprices=candlprices2)