from django.db import models

# Create your models here.
#Контейнерные, формовые, масажные,
class Body(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


# Ароматические и нет
class Flavoring(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Color(models.Model):
    name = models.CharField(max_length=32)


    def __str__(self):
        return self.name

    def candlname_count(self):
        return CandlName.objects.filter(color=self).count()


    def flavoring_status(self):
        flavors = self.flavoring.count()
        if flavors == 1:
            return 'One flavor'
        elif flavors == 2:
            return 'Two flavors'
        elif flavors >= 3:
            return 'More than three'
        else:
            return 'No Aroma'


class CandlPrices(models.Model):
    name = models.IntegerField()

    def __int__(self):
        return self.name


class CandlName(models.Model):
    name = models.CharField(max_length=32, unique=True)
    body = models.ForeignKey(Body, on_delete=models.CASCADE)
    color = models.ForeignKey(Color, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='candel', blank=True, null=True)
    flavoring = models.ManyToManyField(Flavoring)
    candlprices = models.OneToOneField(CandlPrices, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


#class Image(models.Model):
 #   img = models.ImageField(upload_to='album')
  #  candl = models.ForeignKey(CandlName, on_delete=models.CASCADE)

class Card(models.Model):
    name = models.TextField()
    candl_name = models.OneToOneField(CandlName, on_delete=models.CASCADE)

    def __str__(self):
        return self.name