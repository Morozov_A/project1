from django.test import TestCase
from .models import CandlName, Color, Body
from user.models import MyUser


class TestCandelListView(TestCase):

    def test_context(self):
        response = self.client.get('/')
        self.assertIn('contact_info', response.context)
        self.assertEquals(response.context['contact_info'], 'тел: 9589484')

    def test_status_code(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)

    def test_content(self):
        response = self.client.get('/')
        self.assertIn('Вход', response.content.decode(encoding='utf-8'))


class TestAnimalUpdateView(TestCase):

    def setUp(self):
        body = Body.objects.create(name='Геометрическая')
        color = Color.objects.create(name='Желтый', body=body)
        self.candel_id = CandlName.objects.create(name='Обычная', color=color).id
        self.user = MyUser.objects.create_user(username='user', password='user123456', email='user@user.com')
        self.user = MyUser.objects.create_superuser(username='admin', password='user123456', email='admin@admin.com')

    def tearDown(self):
        print('Тест завршен')


    def test_perms(self):
        response = self.client.get(f'/update/{self.candel_id}/')
        self.assertEquals(response.status_code, 302)

        self.client.login(username='user', password='user123456')

        response = self.client.get(f'/update/{self.candel_id}/')
        self.assertEquals(response.status_code, 403)


    def test_super_perms(self):
        self.client.login(username='admin', password='user123456')
        response = self.client.get(f'/update/{self.candel_id}/')
        self.assertEquals(response.status_code, 200)