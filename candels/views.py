from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render
from .models import CandlName
from django.views.generic import ListView, TemplateView, DetailView, \
    CreateView, UpdateView, DeleteView
from .forms import CandelForm

class CandelDeleteView(DeleteView):

    model = CandlName
    template_name = 'candels/delete_confirm.html'
    success_url = '/'
    form_class = CandelForm

class CandelUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'candels.change_candlname'
    model = CandlName
    template_name = 'candels/create.html'
    success_url = '/'
    form_class = CandelForm


class CandelCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'candels.add_candlname'
    model = CandlName
    template_name = 'candels/create.html'
    success_url = '/'
    form_class = CandelForm

class CandelListView(ListView):
    model = CandlName
    template_name = 'candels/index.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context['contact_info'] = 'тел: 9589484'
        context['about_ass'] = 'gutfmhgfg'
        return context

    def get_queryset(self):
        return CandlName.objects.select_related('body', 'color', 'card')

    def count_flaver(self):
        flavors = CandlName.objects.prefetch_related('flavoring')
        flowerlen = len(flavors)
        return flowerlen

class AboutTemplateView(TemplateView):
    template_name = 'candels/about.html'
# Create your views here.

class CandelDetailView(DetailView):
    model = CandlName
    template_name = 'candels/candel.html'

    def get_queryset(self):
        return CandlName.objects.prefetch_related('flavoring')



def index_view(request):
    # candl_name = CandlName.objects.all()

    candl_name = CandlName.objects.select_related('candel', 'candl__body')
    # select_related - когда ForeignKey
    # prefetch_relate - когда ManyToMany


    context = {
        'candl_name': candl_name,
        'candls_Magaz': 'Свечи'
    }


    if request.method =="POST":
        with open('candels.txt', 'w', encoding='utf-8') as f:
            for candel in candl_name:
                f.write(candel.name + '\n')

    return render(request, 'candels/index.html', context=context)
    # flask
    # return render(request, 'animals/index.html', animals=animals, zoo_name='Три поросенка')
    # return render(request, 'animals/index.html', **context)